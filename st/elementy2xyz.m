#!/usr/bin/octave -q

% Ten skrypt zawiera zestaw komend pozwalających na
% obliczenie pozycji satelity na podstawie pliku
% nawigacyjnego

% Należy go uruchomić przy pomocy programu
%   Octave (darmowy odpowiednik Matlaba)
% lub
%   Matlab (nie testowałem, może wymagać małych poprawek)

% wybierz plik nawigacyjny i podaj jego adres
input_file = "./rnx/joze0440.13n";

% adresy do odpowiednich funkcji: 
% komenda wget pozwoli na pobranie niezbędnych funkcji,
% których autorem jest Kai Borre
% proszę prześledzić ich działanie
% można również pliki pobrać ręcznie ze wskazanych adresów
% system("wget -nc http://kom.aau.dk/~borre/matlab/proc_dd/rinexe.m -O /home/mrajner/src/borre_gps/rinexe.m")
% system("wget -nc http://kom.aau.dk/~borre/life-l99/get_eph.m -O /home/mrajner/src/borre_gps/get_eph.m")
% system("wget -nc http://kom.aau.dk/~borre/life-l99/find_eph.m -O /home/mrajner/src/borre_gps/find_eph.m")
% system("wget -nc http://kom.aau.dk/~borre/life-l99/julday.m -O /home/mrajner/src/borre_gps/julday.m")
% system("wget -nc http://kom.aau.dk/~borre/life-l99/* -r -np -nd -P /home/mrajner/src/borre_gps/")

%% dane nawigacyjne
% system("wget -nc ftp://cddis.nasa.gov/gps/data/daily/2013/044/13n/joze0440.13n.Z -P rnx/ && cd rnx; gunzip joze0440*Z")


% dodaj ścieżke z funkcjami
% można je również umieścic w tym samym katalogu
% co ten skrypt i będą one widoczne dla programu
% wtedy zakomentuj poniższą linię
addpath ("/home/mrajner/src/borre_gps/")


% plik nawigacyjny w formie binarnej, żeby przyspieszyć kolejne działania
output_file = strcat(input_file,"eph");


% jeżeli plik binarny nie istnieje to go stwórz z pliku RINEX
% Octave może wymagać zakomentowanie linie
%   spare = line(42:60);
%   spare = line(61:79);
% w pliku rinexe.m jeżeli te pola są puste
if (exist (output_file,"file") != 2) 
    rinexe ( input_file, output_file ) ;
end

% wczytaj plik binarny
eph = get_eph(output_file) ;

if (nargin < 1) 
  numery   = [0:45];
  satelity = unique(sort(eph(1,:)));
  szczegoly='t';
elseif(argv(){1} == "-i")
  numery    = input ("numer(y): ");
  satelity  = input ("satelita(y): ");
  szczegoly = input ("szczegóły [t/n]: ", "s");
else
  display "wywołaj bez argumentów lub z -i (interactive)";
  exit 
end


% zadana data
rok     = 2013 ;
miesiac =    2 ;
dzien   =   13 ;

% uzmiennienie w zależności od numeru
for nr = numery
  % i godzina
  godzina = 0;
  minuta  = 5;
  minuta  = minuta + (15*nr);
  while (minuta > 60)
    minuta  = minuta-60;
    godzina = godzina +1;
  end
  godzina_dziesietna = godzina + minuta/60;

  printf ("\n%s%d%s%d%s%d%s\n" , "==== nr = ",nr,"==== Godzina: ",godzina,":",minuta," ===================")
  % oblicz dzień juliański dla zadanego momentu
  jd = julday( rok , miesiac , dzien , godzina_dziesietna);

  % na podstawie jd oblicz sekundy od początku danego tygodnia GPS
  [tydzien_gps, sekundy_gps]=gps_time (jd);


  if (szczegoly!='t')
    printf ("%4s %14s %14s %14s\n" , "prn",  "X [m]" , "Y [m]", "Z [m]")
  end

  for prn = satelity %unique(sort(eph(1,:)))
    my_eph_col = find_eph( eph, prn , sekundy_gps );

    if (szczegoly=='t')
      printf ("%4s = %4d\n" ,"prn", prn)
    end

    % oblicz pozycję satelity
    X =  satpos  (sekundy_gps, eph(:,my_eph_col), szczegoly);

    if (szczegoly!='t')
      % Wydrukuj wyniki
      printf ("%4d %14.2f %14.2f %14.2f\n" , prn,  X(1) , X(2), X(3))
    end

  end
end
